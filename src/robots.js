export const robots = [
    {
        id: 1,
        name: 'Biff Biffson',
        username: 'biff',
        email: 'biff@biffsonindustries.biz'
    },
    {
        id: 2,
        name: 'Noah Biggleton',
        username: 'noah',
        email: 'designforlife@riddlebox.io'
    },
    {
        id: 3,
        name: 'Per Anderson',
        username: 'sweper',
        email: 'perand@sweden.net'
    },
    {
        id: 4,
        name: 'Punta Laaksi',
        username: 'greyhound55',
        email: 'ubeenhaxd@gmail.com'
    },
    {
        id: 5,
        name: 'Ron Burgundy',
        username: 'theonlyron',
        email: 'ron@channel5news.com'
    },
    {
        id: 6,
        name: 'Ron Howie',
        username: 'theonlyron2',
        email: 'ron@gmail.com'
    },
    {
        id: 80,
        name: 'Eighty ID',
        username: 'id80',
        email: 'id80@unittest.com'
    },
    {
        id: 99,
        name: 'Same ID as Noah',
        username: 'theIDisalreadytaken',
        email: 'blah'
    },
    {
        id: 7,
        name: 'Bubba Haybale',
        username: 'mahsistahismahwife',
        email: 'redneck@farm.net'
    }

];